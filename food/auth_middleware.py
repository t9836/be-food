from rest_framework.response import Response
from rest_framework import status
from django.utils.deprecation import MiddlewareMixin
from django.http import JsonResponse

import requests

class LoginRequiredMiddleware(MiddlewareMixin):
  def __init__(self, get_response):
      self.get_response = get_response
  def process_request(self, request):
    token = request.headers.get("Authorization")
    if not token:
      return JsonResponse({
          'error': 'invalid_request',
          'error_description': [
              'No credentials!'
          ]
      }, status=status.HTTP_403_FORBIDDEN)
    else:
      headers = {
          'Content-Type': 'application/json',
          'Authorization': token
      }
      r = requests.post('https://oauth-a7law.herokuapp.com/oauth/resource', headers=headers)
      data = r.json()
      try:
          if data['error'] == 'invalid_request':
              return JsonResponse({
                  'error': 'invalid_request',
                  'error_description': [
                      'Credentials not valid!!'
                  ]
              }, status=status.HTTP_403_FORBIDDEN)
      except KeyError:
          pass

    return self.get_response(request)