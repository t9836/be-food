from django.db import models

# Create your models here.
class Food(models.Model):
    nama = models.CharField(max_length=255)
    harga = models.CharField(max_length=255)
    deskripsi = models.CharField(max_length=255)
    foto = models.ImageField(null=True, blank=True)
    penjual = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

# Constants
REVIEW_STATUS_UPLOADING = "UPLOADING"
REVIEW_STATUS_PUBLISHED = "PUBLISHED"
REVIEW_STATUS_ERROR = "ERROR"

REVIEW_STATUS_CHOICES = (
    (REVIEW_STATUS_UPLOADING, "Uploading"),
    (REVIEW_STATUS_PUBLISHED, "Published"),
    (REVIEW_STATUS_ERROR, "Error")
)


# Food review model
class FoodReview(models.Model):
    order_id = models.CharField(max_length=255)
    food_id = models.CharField(max_length=255)
    komentar = models.CharField(max_length=255)
    url_video = models.CharField(max_length=255, null=True, blank=True)
    url_thumbnail = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=9, choices=REVIEW_STATUS_CHOICES)

    def __str__(self):
        return self.order_id + "_" + self.food_id