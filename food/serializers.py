from rest_framework import serializers
from .models import Food, FoodReview

class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = [
            'id', 'nama', 'harga', 'deskripsi',
            'foto', 'penjual'
        ]

class FoodReviewPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodReview
        fields = [
            'id', 'order_id', 'food_id', 'komentar',
            'url_video', 'status'
        ]

class FoodReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodReview
        fields = [
            'id', 'order_id', 'food_id', 'komentar',
            'url_video', 'url_thumbnail'
        ]
