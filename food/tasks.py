from befood.celery import app
from .models import FoodReview, REVIEW_STATUS_PUBLISHED, REVIEW_STATUS_ERROR

import base64
import cloudinary.uploader


def upload_video_to_cloudinary(video):
    """Uploads video to Cloudinary"""
    return cloudinary.uploader.upload(
      video,
      folder='reviews',
      resource_type='video'
    )


@app.task(bind=True)
def upload_video_review_task(self, video_base64, review_id):
    """Perform an upload review task"""
    review_instance = FoodReview.objects.get(id=review_id)

    try:
      decoded_video = base64.b64decode(video_base64)
      upload_data = upload_video_to_cloudinary(decoded_video)
      review_instance.url_video = upload_data["secure_url"]
      review_instance.url_thumbnail = upload_data["secure_url"][:-3] + "jpg"
      review_instance.status = REVIEW_STATUS_PUBLISHED
    except Exception as e:
      review_instance.status = REVIEW_STATUS_ERROR
    
    review_instance.save()
    


    
    
