from django.urls import path
from .views import (
    FoodView, FoodDetailView,
    FoodOrderReviewView, FoodDetailReviewView, FoodSellerView
)

app_name = 'food'
urlpatterns = [
    path('food/', FoodView.as_view(), name='url_food'),
    path('food/seller', FoodSellerView.as_view(), name='url_food_seller'),
    path('food/<int:food_id>',
         FoodDetailView.as_view(),
         name='url_food_detail'
    ),
    path('review/order/<str:order_id>',
         FoodOrderReviewView.as_view(),
         name='url_food_order_review'
    ),
    path('review/food/<str:food_id>',
         FoodDetailReviewView.as_view(),
         name='url_food_detail_review'
    ),
]
