from .models import Food, FoodReview, REVIEW_STATUS_UPLOADING, REVIEW_STATUS_PUBLISHED
from .serializers import FoodSerializer, FoodReviewSerializer, FoodReviewPostSerializer

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .tasks import upload_video_review_task
import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

import base64
import cloudinary.uploader
import requests

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)

# Create the handler
handler = AsynchronousLogstashHandler(
    host='343e0cf4-eb64-4642-8aed-f8a94ed80b47-ls.logit.io',
    port=10112,
    ssl_enable=False,
    ssl_verify=False,
    database_path='')
# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)

# Create your views here.
class FoodView(APIView):
    def get(self, request, format=None):
        foods = Food.objects.all().order_by('nama')
        serializer = FoodSerializer(foods, many=True)
        logger.info('Get Food')
        return Response(serializer.data, status=200)

    def post(self, request, format=None):
        serializer = FoodSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)
        logger.info('Create Food', extra={'nama': self.request.data.get("nama")})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FoodSellerView(APIView):
    def get(self, request, format=None):

        penjual = request.query_params.get('penjual')

        if not penjual:
            return Response('Form Error', status=400)



        foods = Food.objects.all().order_by('nama').filter(penjual=penjual)
        serializer = FoodSerializer(foods, many=True)

        logger.info('Seller Food', extra={'penjual': penjual})
        return Response(serializer.data, status=200)

class FoodDetailView(APIView):
    def get(self, request, food_id, format=None):
        try:
            food = Food.objects.get(id=food_id)
            serializer = FoodSerializer(food)
            logger.info('Specific Food', extra={'food_id': food_id})
            return Response(serializer.data)
        except Food.DoesNotExist:
            return Response(
                {'response': 'Food with id: ' + str(food_id) + ' not found'},
                status=404
            )

    def patch(self, request, food_id, format=None):
        try:
            response = request.data
            food = Food.objects.get(id=food_id)
            params = [
                'id', 'nama', 'harga', 'deskripsi',
                'foto', 'penjual'
            ]

            for param in params:
                if param in response:
                    if response[param] != None or response[param] != "":
                        setattr(food, param, response[param])

            food.save()
            serializer = FoodSerializer(food)
            logger.info('Specific Food Update', extra={'food_id': food_id})
            return Response(serializer.data, status=200)
        except Food.DoesNotExist:
            return Response(
                {'response': 'Food with id: ' + str(food_id) + ' not found'},
                status=404
            )

    def delete(self, request, food_id, format=None):
        try:
            food = Food.objects.get(id=food_id)
            food.delete()
            logger.info('Delete Food', extra={'food_id': food_id})
            return Response(
                {'response': 'Food with id: ' + str(food_id) + ' deleted'},
                status=200
            )
        except Food.DoesNotExist:
            return Response(
                {'response': 'Food with id: ' + str(food_id) + ' not found'},
                status=404
            )


class FoodOrderReviewView(APIView):
    def post(self, request, order_id, format=None):
        video = request.data.get("video")
        token = request.headers.get("Authorization")

        headers = {
            'Content-Type': 'application/json',
            'Authorization': token
        }

        if FoodReview.objects.filter(order_id=order_id):
            return Response({
                'error': 'invalid_request',
                'error_description': [
                    'Review for order_id already exists'
                ],
            }, status=status.HTTP_400_BAD_REQUEST)

        r = requests.get(f'https://34.135.73.10/order/buyer/order-detail?order_id={order_id}', headers=headers, verify=False)
        data = r.json()

        try:
            if data['error'] == 'not found':
                return Response({
                    'error': 'invalid_request',
                    'error_description': [
                        data['error_description'],
                    ]
                })
        except KeyError:
            pass

        current_data = {
            'order_id': order_id,
            'food_id': data['data']['id_food'],
            'komentar': request.data.get('komentar'),
            'url_video': None,
            'status': REVIEW_STATUS_UPLOADING
        }

        serializer = FoodReviewPostSerializer(data=current_data)
        if serializer.is_valid():
            review_instance = serializer.save()
            logger.info("Create new review", extra={'order_id': order_id})

            request_body = {
                'order_id': order_id,
            }
            r = requests.post('https://34.135.73.10/order/buyer/review', headers=headers, verify=False, json=request_body)

            video_raw_bytes = video.read()
            video_base64 = base64.b64encode(video_raw_bytes).decode()
            upload_video_review_task.delay(video_base64, review_instance.id)
            return Response({
                'status': 'Success',
                'message': "Review successfully submitted",
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

class FoodDetailReviewView(APIView):
    def get(self, request, food_id, format=None):
        foods = FoodReview.objects.filter(food_id=food_id, status=REVIEW_STATUS_PUBLISHED)
        serializer = FoodReviewSerializer(foods, many=True)
        logger.info('Detail Review Food', extra={'food_id': food_id})
        return Response(serializer.data, status=200)
