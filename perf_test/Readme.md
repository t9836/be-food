# Perfomance Test Using Locust
## For Windows User with Docker
1. Run script file named "runlocustdocker.bat" with number of worker you want. 
    * Example:
    * To run locust with 5 workers:
        ```sh
        runlocustdocker.bat 5
        ```

## For Linux User with Docker
1. Run script file named "shrunlocust.sh" with number of worker you want. 
    * Example:
    * To run locust with 5 workers:
        ```sh
        shrunlocust.sh 5
        ```