from locust import HttpUser, task
from vars import (
    FOOD_ROUTE, POST_FOOD_JSON, HEADERS
)

import json

class SellerUser(HttpUser):
    @task
    def seller_post_food(self):
        self.client.post(FOOD_ROUTE, headers=HEADERS, data=POST_FOOD_JSON)
